package ng.upperlink.javadev2.domainobject;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
@NamedQueries( {
	@NamedQuery(name = "UserDO.findAll", query = "select s from UserDO s order by s.id ")
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDO {
	
	@SequenceGenerator(name="user_id_gen",  sequenceName="user_id_seq", initialValue = 2, allocationSize = 1)
	@Id	@GeneratedValue(generator="user_id_gen")
	private Long id;
	
	@NotNull(message = "First name parameter missing!")
	@Size(min = 2, message = "First name must have minimum of 2 characters!")
	@Column(length = 100, unique = true, nullable = false)
	private String firstname;
	
	@NotNull(message = "Last name parameter missing!")
	@Size(min = 2, message = "Last name must have minimum of 2 characters!")
	@Column(name="lastname", length = 100, unique = true, nullable = false)
	private String surname;
	
	@NotNull(message = "Phone parameter missing!")
	@Size(min = 11, message = "Phone number must have minimum of 11 characters!")
	@Column(length = 100, unique = true, nullable = false)
	private String phone;
	
	@NotNull(message = "Email parameter missing!")
	@Column(length = 100, unique = true, nullable = false)
	private String email;
	
	@NotNull(message = "Password parameter missing!")
	@Column(length = 100, unique = true, nullable = false)
	private String password;
	
	@NotNull(message = "Cover letter parameter missing!")
	@Column(length = 500, unique = true, nullable = false)
	private String coverletter;
	
	@NotNull(message = "Passport parameter missing!")
	@Column(length = 200, unique = true, nullable = false)
	private String passporturl;
	
	@NotNull(message = "Resume parameter missing!")
	@Column(length = 200, unique = true, nullable = false)
	private String resumeurl;
	
	@NotNull(message = "Resume parameter missing!")
	@Column(length = 200, unique = true, nullable = false)
	private boolean admin;
	
	@Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime created;
	
	@Column(nullable = false)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime lastUpdate = ZonedDateTime.now();

}
