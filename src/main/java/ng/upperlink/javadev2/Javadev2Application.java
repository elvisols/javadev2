package ng.upperlink.javadev2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import ng.upperlink.javadev2.util.LogInterceptor;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class Javadev2Application extends WebMvcConfigurerAdapter  {

	public static void main(String[] args) {
		SpringApplication.run(Javadev2Application.class, args);
	}
	
	/**
	 * Activate root interceptors for client/consumer details logging etc.
	 * 
	 */
	@Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(new LogInterceptor()).addPathPatterns("/**");
    }
	
	@Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        super.addViewControllers(registry);
        registry.addViewController("/users").setViewName("forward:/"); // forward to angular defined route
        registry.addViewController("/detail/*").setViewName("forward:/"); // forward to angular defined route
	}
	
	@Bean
    public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver mr = new CommonsMultipartResolver();
//		mr.setMaxUploadSizePerFile(123456789);
        return mr;
    }
	
	/**
	 * Create a springfox documentation bean using SWAGGER2, on the all path.
	 * @see ~/swagger-ui.html
	 */
	@Bean
    public Docket docket()
    {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage(getClass().getPackage().getName()))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(generateApiInfo());
    }


    private ApiInfo generateApiInfo()
    {
        return new ApiInfo("User RESTful Endpoint", "Service Documentaion", "Version 1.0 - mw",
            "urn:tos", "elvisouk@yahoo.com", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0");
    }
	
}
