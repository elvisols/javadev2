package ng.upperlink.javadev2.controller;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ng.upperlink.javadev2.domainobject.UserDO;
import ng.upperlink.javadev2.exception.ConstraintsViolationException;
import ng.upperlink.javadev2.exception.UserNotFoundException;
import ng.upperlink.javadev2.service.UserService;

/**
 * A class to handle all API User requests.
 * This class exposes the USER data repository as REST.
 * 
 * <pre>
 *    Allowed Methods: GET, POST, PUT, DELETE
 * </pre>
 * 
 * @author  Elvis
 * @version 1.0, 25/08/18
 * https://bitbucket.org/elvisols/javadev2/src/master/
 */
@RestController
@RequestMapping("api/users")
//@Api(value = "User")
public class UserController {

	private static int count = 0;
	
	@Autowired
	UserService userService;
	
    @Value("${server.upload.path}")
    private String uploadPath;
	
	/**
	* This method fetches all users
	*
	* @param void
	* @method GET
	* @return result as list of user object
	*/
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Get all Users")
	public List<UserDO> findUsers() {
		return userService.findAllUsers();
	}
	
	/**
	* This method returns a single user record.
	*
	* @param <b>userId</b> this is the id of the user record to retrieve.
	* @method GET
	* @return result, as a user object
	* @throws UserNotFoundException, if user record for the specified id cannot be retrieved.
	*/
	@GetMapping(value="/{userId}", produces=MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Get a single user by id")
	public UserDO findUserById(@PathVariable Long userId) throws UserNotFoundException {
		return userService.findUserById(userId);
	}
	
	/**
	* This method returns a newly created user record.
	*
	* @param <b>userDTO</b> this is the JSON user object to create.
	* @method POST
	* @return result, as a user object
	* @throws ConstraintsViolationException, if JSON user object violates some constraints.
	*/
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
//	@ApiOperation(value = "Create a user")
	public ResponseEntity<Map<String, Object>> addUser(
			@RequestParam(value = "passport") MultipartFile passport, 
    		@RequestParam(value = "resume") MultipartFile resume,
			@Valid @RequestBody UserDO userDO) throws ConstraintsViolationException {
		Map<String, Object> r = new HashMap<>();
		 if (!passport.isEmpty() && !resume.isEmpty() ) {
			 try {
				  System.out.println("Passport: " + passport.getContentType() + " Resume: " + resume.getContentType());
				  String passportFilename = passport.getOriginalFilename();
				  String resumeFilename = resume.getOriginalFilename();
				  String directoryName = uploadPath +  File.separator + userDO.getEmail();
				  File directory = new File(directoryName);
                  if (! directory.exists()){
				      directory.mkdir();
				  }
				  File passportDestinationFile = new File(directoryName + File.separator + passportFilename);
				  passport.transferTo(passportDestinationFile);
				  
				  userDO.setPassporturl(directoryName + File.separator + passportFilename);
				  
				  File resumeDestinationFile = new File(directoryName + File.separator + resumeFilename);
				  resume.transferTo(resumeDestinationFile);
				  
				  userDO.setResumeurl(directoryName + File.separator + resumeFilename);
				  
				  r.put("status", 200);
				  r.put("message", "Account created");
				  
				  /**** Update user account table  *****/
				  userService.createUser(userDO);
				  
				  count++;
				  
				  return new ResponseEntity<Map<String, Object>>(r, HttpStatus.OK);
				  
			 } catch (Exception e) {  
				 r.put("status", 500);
				 r.put("message", "Error! creating account - " + e.getMessage());
				 return new ResponseEntity<Map<String, Object>>(r, HttpStatus.INTERNAL_SERVER_ERROR);
			 }
		 } else {
			 r.put("status", 200);
			 String message = count == 4 ? "Application closed" : "Bad or incomplete parameter request";
			 r.put("message", message);
			 return new ResponseEntity<Map<String, Object>>(r, HttpStatus.BAD_REQUEST);
			 
		 }
	}
	
	/**
	* This method returns a single updated user record.
	*
	* @param <b>userId</b> this is the id of the user record to update.
	* @param <b>userDTO</b> this is the JSON user object to update.
	* @method PUT
	* @return result, as a user object
	* @throws UserNotFoundException | ConstraintsViolationException, if user record for the specified id cannot be retrieved or constraint(s) is violated.
	*/
	@PutMapping(value="/{userId}", produces=MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Update a user with the specified id")
	public UserDO updateUser(@Valid @PathVariable long userId, @RequestBody UserDO userDO) throws ConstraintsViolationException, UserNotFoundException {
		userDO.setId(userId);	// set user id to update
		return userService.updateUser(userDO);
	}
	
	/**
	* This method "hard" deletes a single user record.
	*
	* @param <b>userId</b> this is the id of the user record to delete.
	* @method DELETE
	* @return void
	* @throws UserNotFoundException if user record for the specified id cannot be retrieved.
	*/
	@DeleteMapping(value="/{userId}", produces=MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "Delete a user with a particular id")
	public void deleteUser(@Valid @PathVariable long userId) throws UserNotFoundException {
		userService.deleteUser(userId);
//		return new ResponseEntity<ResponseWrapper>(new ResponseWrapper(HttpStatus.OK.value(), "Deleted user with an ID: " + userId), HttpStatus.OK);
	}
	
}
