package ng.upperlink.javadev2.controller.mapper;


/**
 * This class provides a Mapper between the user's data object (DO) and the data transfer object (DTO).
 * 
 * @author  Elvis
 * @version 1.0, 25/08/18
 */
public class UserMapper {

//	/**
//	* This method maps the user's DTO to DO.
//	*
//	* @param <b>userDTO</b> this is the user object gotten from client/consumer.
//	* @return UserDO
//	*/
//    public static UserDO makeUser(UserDTO userDTO)
//    {
//        UserDO user = new UserDO();  // populate transactions
//        return user;
//    }
//    
//    /**
//	* This method maps the user's DO to DTO.
//	*
//	* @param <b>userDO</b> this is the user object gotten from the application.
//	* @return UserDTO
//	*/
//    public static UserDTO makeUserDTO(UserDO user)
//    {
//        UserDTO.UserDTOBuilder userDTOBuilder = UserDTO.newBuilder()
//            .setId(user.getId())
//            .setLastUpdate(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a").format(user.getLastUpdate()));
//
//        return userDTOBuilder.createUserDTO();
//    }
//    
//    /**
//	* This method creates a collection of all users DTO.
//	*
//	* @param <b>users</b> this is the user object collection from the application.
//	* @return List<UserDTO>, this streams and maps all users in the collection to DTO and return the list to client/consumer.
//	*/
//    public static List<UserDTO> makeUserDTOList(Collection<UserDO> users)
//    {
//        return users.stream()
//            .map(UserMapper::makeUserDTO)
//            .collect(Collectors.toList());
//    }
//    
}
