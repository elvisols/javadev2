package ng.upperlink.javadev2.service;

import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ng.upperlink.javadev2.dataaccessobject.UserRepository;
import ng.upperlink.javadev2.domainobject.UserDO;
import ng.upperlink.javadev2.exception.ConstraintsViolationException;
import ng.upperlink.javadev2.exception.UserNotFoundException;


@Service
public class UserServiceImpl implements UserService {

	private static org.slf4j.Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository; 
    
	@Override
	public UserDO findUserById(Long userId) throws UserNotFoundException {
		LOG.info("...Getting user with Id: ", userId);
		return userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException("Oops! Could not find user entity with id:" + userId));
	}

	@Override
	public UserDO createUser(UserDO userDO) throws ConstraintsViolationException {
		LOG.info("...Saving user: ", userDO);
		return userRepository.save(userDO);
	}

	@Override
	public UserDO updateUser(UserDO userDO) throws ConstraintsViolationException, UserNotFoundException {
		LOG.info("...Updating: ", userDO);
		UserDO s = this.findUserById(userDO.getId());
		// Update user's records accordingly.
		s.setFirstname(userDO.getFirstname() == null ? s.getFirstname() : userDO.getFirstname());
		s.setLastUpdate(ZonedDateTime.now());
		LOG.info("...Updating user with id ", userDO.getId());
		return userRepository.save(s);
	}

	@Override
	public void deleteUser(Long userId) throws UserNotFoundException {
		LOG.warn("...Deleting user with Id: ", userId);
		userRepository.deleteById(userId);
	}

	@Override
	public List<UserDO> findAllUsers() {
		LOG.info("...Fetching users");
		return (List<UserDO>) userRepository.findAll();
	}

}
