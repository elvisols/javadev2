package ng.upperlink.javadev2.service;

import java.util.List;

import ng.upperlink.javadev2.domainobject.UserDO;
import ng.upperlink.javadev2.exception.ConstraintsViolationException;
import ng.upperlink.javadev2.exception.UserNotFoundException;


public interface UserService {

	UserDO findUserById(Long userId) throws UserNotFoundException;

	UserDO createUser(UserDO userDO) throws ConstraintsViolationException;
    
	UserDO updateUser(UserDO userDO) throws ConstraintsViolationException, UserNotFoundException;

    void deleteUser(Long userId) throws UserNotFoundException;
    
    List<UserDO> findAllUsers();
    
}
