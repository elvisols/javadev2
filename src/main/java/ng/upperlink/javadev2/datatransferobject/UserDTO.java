package ng.upperlink.javadev2.datatransferobject;

import java.time.ZonedDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ng.upperlink.javadev2.domainobject.UserDO;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

	private Long id;
	
	@NotNull(message = "First name parameter missing!")
	@Size(min = 2, message = "First name must have minimum of 2 characters!")
	private String firstname;
	
	@NotNull(message = "Last name parameter missing!")
	@Size(min = 2, message = "Last name must have minimum of 2 characters!")
	private String surname;
	
	@NotNull(message = "Phone parameter missing!")
	@Size(min = 11, message = "Phone number must have minimum of 11 characters!")
	private String phone;
	
	@NotNull(message = "Email parameter missing!")
	private String email;
	
	@NotNull(message = "Password parameter missing!")
	private String password;
	
	@NotNull(message = "Cover letter parameter missing!")
	private String coverletter;
	
	@NotNull(message = "Passport url parameter missing!")
	private String passporturl;
	
	@NotNull(message = "Resume parameter missing!")
	private String resumeurl;
	
	@NotNull(message = "admin parameter missing!")
	private boolean admin;
	
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime created;
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime lastUpdate = ZonedDateTime.now();
	
	public static UserDTOBuilder newBuilder() {
		return new UserDTOBuilder();
	}
	
	public static class UserDTOBuilder {
	}
	
}
