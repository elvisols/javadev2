package ng.upperlink.javadev2.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A class that captures exception thrown when a user entity is missing in the database.
 *
 * @author  Elvis
 * @version 1.0, 25/08/18
 * @see     java.lang.Exception
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Oops! Could not find user entity with id")
public class UserNotFoundException extends Exception
{
    static final long serialVersionUID = -3387516993334229948L;

    /**
	* This constructor allows you to override the default message on missing user exception thrown
	*
	* @param <b>message</b> this is the exception message
	* @exception UserNotFoundException
	*/
    public UserNotFoundException(String message)
    {
        super(message);
    }

}
