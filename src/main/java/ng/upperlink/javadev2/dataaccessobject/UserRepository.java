package ng.upperlink.javadev2.dataaccessobject;

import org.springframework.data.repository.CrudRepository;

import ng.upperlink.javadev2.domainobject.UserDO;


// Use JPA Data for basic Crud Jobs
public interface UserRepository extends CrudRepository<UserDO, Long> {

}
