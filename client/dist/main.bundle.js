webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__stocks_stocks_component__ = __webpack_require__("../../../../../src/app/stocks/stocks.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__stock_detail_stock_detail_component__ = __webpack_require__("../../../../../src/app/stock-detail/stock-detail.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: '', redirectTo: '/stocks', pathMatch: 'full' },
    { path: 'detail/:id', component: __WEBPACK_IMPORTED_MODULE_3__stock_detail_stock_detail_component__["a" /* StockDetailComponent */] },
    { path: 'stocks', component: __WEBPACK_IMPORTED_MODULE_2__stocks_stocks_component__["a" /* StocksComponent */] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forRoot(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Stock List';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__stocks_stocks_component__ = __webpack_require__("../../../../../src/app/stocks/stocks.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__stock_detail_stock_detail_component__ = __webpack_require__("../../../../../src/app/stock-detail/stock-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__stock_service__ = __webpack_require__("../../../../../src/app/stock.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_5__stocks_stocks_component__["a" /* StocksComponent */],
                __WEBPACK_IMPORTED_MODULE_6__stock_detail_stock_detail_component__["a" /* StockDetailComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_8__app_routing_module__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_7__stock_service__["a" /* StockService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/stock-detail/stock-detail.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".img-responsive {\r\n\tmax-width: 80%\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/stock-detail/stock-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<!--banner-->\n<div class=\"banner-top\">\n\t<div class=\"container\">\n\t\t<h3 >{{title}}</h3>\n\t\t<h4><a routerLink=\"/stocks\">List All Stock</a><label>/</label>{{title}}</h4>\n\t\t<div class=\"clearfix\"> </div>\n\t</div>\n</div>\n<div class=\"single\">\n\t<div class=\"container\">\n\t\t<div class=\"single-top-main\">\n\t\t\t<div class=\"col-md-5 single-top\">\n\t\t\t\t<div class=\"single-w3agile\">\n\n\t\t\t\t\t<div id=\"picture-frame\">\n\t\t\t\t\t\t<img src=\"assets/images/warehouse.jpg\" data-src=\"assets/images/warehouse.jpg\" alt=\"\" class=\"img-responsive\"/>\n\t\t\t\t\t</div>\n\t\t\t\t\t<script src=\"js/jquery.zoomtoo.js\"></script>\n\t\t\t\t\t<script>\n\t\t\t\t\t\t$(function() {\n\t\t\t\t\t\t\t$(\"#picture-frame\").zoomToo({\n\t\t\t\t\t\t\t\tmagnify: 1\n\t\t\t\t\t\t\t});\n\t\t\t\t\t\t});\n\t\t\t\t\t</script>\n\n\n\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-md-7 single-top-left \">\n\t\t\t\t<div class=\"single-right\" *ngIf=\"stock\">\n\n\t\t\t\t\t<h3>{{ stock.name | uppercase }} Details</h3>\n\t\t\t\t\t<hr />\n\t\t\t\t\t<div><span>id: </span>{{stock.id}}</div>\n\t\t\t\t\t<div><span>lastUpdated: </span>{{stock.lastUpdate}}</div>\n\t\t\t\t\t<hr />\n\t\t\t\t\t<table>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td>Stock Name:</td>\n\t\t\t\t\t\t\t<td><input [(ngModel)]=\"stock.name\" placeholder=\"stock name\" /><br /></td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td>Stock Price:</td>\n\t\t\t\t\t\t\t<td><input type=\"number\" [(ngModel)]=\"stock.currentPrice\" placeholder=\"stock current price\" /></td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td>Stock Quantity:</td>\n\t\t\t\t\t\t\t<td><input type=\"number\" [(ngModel)]=\"stock.quantity\" placeholder=\"stock quantity\" /></td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</table>\n\t\t\t\t\t<hr />\n\t\t\t\t\t<div class=\"add add-3\">\n\t\t\t\t\t\t<button class=\"btn btn-danger my-cart-btn my-cart-b\" (click)=\"goBack()\">< go back</button>\n\t\t\t\t\t\t<button class=\"btn btn-danger my-cart-btn my-cart-b\" (click)=\"save()\">Update</button>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"clearfix\"> </div>\n\t\t\t\t</div>\n\n\n\t\t\t</div>\n\t\t\t<div class=\"clearfix\"> </div>\n\t\t</div>\t\n\n\n\t</div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/stock-detail/stock-detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StockDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__stock__ = __webpack_require__("../../../../../src/app/stock.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__stock_service__ = __webpack_require__("../../../../../src/app/stock.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StockDetailComponent = /** @class */ (function () {
    function StockDetailComponent(route, stockService, location) {
        this.route = route;
        this.stockService = stockService;
        this.location = location;
        this.title = "View-Edit";
    }
    StockDetailComponent.prototype.ngOnInit = function () {
        this.getStock();
    };
    StockDetailComponent.prototype.getStock = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.stockService.getStock(id)
            .subscribe(function (stock) { return _this.stock = stock; });
    };
    StockDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    StockDetailComponent.prototype.save = function () {
        var _this = this;
        this.stockService.updateStock(this.stock)
            .subscribe(function () { return _this.goBack(); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__stock__["a" /* Stock */])
    ], StockDetailComponent.prototype, "stock", void 0);
    StockDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-stock-detail',
            template: __webpack_require__("../../../../../src/app/stock-detail/stock-detail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/stock-detail/stock-detail.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_4__stock_service__["a" /* StockService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["f" /* Location */]])
    ], StockDetailComponent);
    return StockDetailComponent;
}());



/***/ }),

/***/ "../../../../../src/app/stock.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StockService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__ = __webpack_require__("../../../../rxjs/_esm5/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__("../../../../rxjs/_esm5/operators.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json', 'Authorization': 'Basic ' + btoa("client:secret") })
};
var StockService = /** @class */ (function () {
    function StockService(http) {
        this.http = http;
        this.stocksUrl = 'api/stocks';
    }
    /** GET stocks from the server */
    StockService.prototype.getStocks = function () {
        return this.http.get(this.stocksUrl)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["b" /* tap */])(function (stocks) { return console.log('Stocks fetched: ' + JSON.stringify(stocks)); }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["a" /* catchError */])(this.handleError('getStockes', [])));
    };
    /** POST: add a new stock to the server */
    StockService.prototype.addStock = function (stock) {
        return this.http.post(this.stocksUrl, stock, httpOptions).pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["b" /* tap */])(function (stock) { return console.log("id=${stock.id} stock added"); }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["a" /* catchError */])(this.handleError('addStock')));
    };
    /** DELETE: delete the stock from the server */
    StockService.prototype.deleteStock = function (stock) {
        var id = typeof stock === 'number' ? stock : stock.id;
        var url = this.stocksUrl + "/" + id;
        return this.http.delete(url, httpOptions).pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["b" /* tap */])(function (_) { return console.log('deleted stock id=${id}'); }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["a" /* catchError */])(this.handleError('deleteStock')));
    };
    /** PUT: update the stock on the server */
    StockService.prototype.updateStock = function (stock) {
        var url = this.stocksUrl + "/" + stock.id;
        return this.http.put(url, stock, httpOptions).pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["b" /* tap */])(function (_) { return console.log('updated stock id=${stock.id}'); }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["a" /* catchError */])(this.handleError('updateStock')));
    };
    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    StockService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            console.log(operation + ' failed: ' + error.message);
            // Let the app keep running by returning an empty result.
            return Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_observable_of__["a" /* of */])(result);
        };
    };
    /** GET stock by id. Will 404 if id not found */
    StockService.prototype.getStock = function (id) {
        var url = this.stocksUrl + "/" + id;
        return this.http.get(url).pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["b" /* tap */])(function (_) { return console.log("fetched stock id=" + id); }), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["a" /* catchError */])(this.handleError("getStock id=${id}")));
    };
    StockService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], StockService);
    return StockService;
}());



/***/ }),

/***/ "../../../../../src/app/stock.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Stock; });
var Stock = /** @class */ (function () {
    function Stock() {
    }
    return Stock;
}());



/***/ }),

/***/ "../../../../../src/app/stocks/stocks.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/stocks/stocks.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- contact -->\n<div class=\"check-out\">\t\n\t<div class=\"container\">\t \n\t\t<div class=\"spec \">\n\t\t\t<h3>{{title}}</h3>\n\t\t\t<div class=\"ser-t\">\n\t\t\t\t<b></b>\n\t\t\t\t<span><i></i></span>\n\t\t\t\t<b class=\"line\"></b>\n\t\t\t</div>\n\n\t\t</div>\n\t\t<a class=\" add-1\" href=\"#\"  data-toggle=\"modal\" data-target=\"#createModal\">Create Stock</a>\n\t\t<br />\n\t\t<br />\n\n\t\t<table class=\"table \">\n\t\t\t<tr>\n\t\t\t\t<th class=\"t-head head-it \"><div align=\"center\">Products</div></th>\n\t\t\t\t<th class=\"t-head\"><div>Current Price</div></th>\n\t\t\t\t<th class=\"t-head\"><div>Quantity</div></th>\n\t\t\t\t<th class=\"t-head\" colspan=\"2\"><div align=\"center\">Actions</div></th>\n\t\t\t</tr>\n\n\n\n\t\t\t<tr class=\"cross\" *ngFor=\"let stock of stocks\">\n\t\t\t\t<td class=\"ring-in t-data\">\n\t\t\t\t\t<a routerLink=\"/detail/{{stock.id}}\" class=\"at-in\">\n\t\t\t\t\t\t<img src=\"assets/images/of{{stock.id}}.png\" class=\"img-responsive\" alt=\"\">\n\t\t\t\t\t</a>\n\t\t\t\t\t<div class=\"sed\">\n\t\t\t\t\t\t<h5>{{stock.name}}</h5>\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t</td>\n\t\t\t\t<td class=\"t-data\">${{stock.currentPrice}}</td>\n\t\t\t\t<td class=\"t-data\">{{stock.quantity}}</td>\n\t\t\t\t<td class=\"t-data\">\n\t\t\t\t\t<a class=\" add-1\" routerLink=\"/detail/{{stock.id}}\">Edit</a>\n\t\t\t\t</td>\n\t\t\t\t<td class=\"t-data t-w3l\">\n\t\t\t\t\t<div class=\"clearfix\"> </div>\n\t\t\t\t\t<div class=\"close1\" (click)=\"delete(stock)\"> <i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>\n\t\t\t\t</td>\n\n\t\t\t</tr>\n\t\t</table>\n\t</div>\n</div>\n\n<!-- Create Stock Modal -->\n<div class=\"modal fade\" id=\"createModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">\n\t\t<div class=\"modal-dialog\" role=\"document\">\n\t\t\t<div class=\"modal-content modal-info\">\n\t\t\t\t<div class=\"modal-header\">\n\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\t\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t\t<div class=\"modal-body modal-spa\">\n\t\t\t\t\t<div class=\"col-md-5 span-2\">\n\t\t\t\t\t\t<div class=\"item\">\n\t\t\t\t\t\t\t<img src=\"assets/images/LogisticsVismaBlue.png\" class=\"img-responsive\" alt=\"\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-md-7 span-1 \">\n\t\t\t\t\t\t<h3>Create a new Stock</h3>\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\t<table cellpadding=\"9\" cellspacing=\"5\">\n\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t<td>Stock Name:</td>\n\t\t\t\t\t\t\t\t\t<td><input #stockName /><br /></td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t<td>Stock Price:</td>\n\t\t\t\t\t\t\t\t\t<td><input type=\"number\" #stockPrice /></td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t<td>Stock Quantity:</td>\n\t\t\t\t\t\t\t\t\t<td><input type=\"number\" #stockQty /></td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"add-to\">\n\t\t\t\t\t\t\t<button class=\"btn btn-danger my-cart-btn my-cart-btn1\" (click)=\"add(stockName.value, stockPrice.value, stockQty.value); stockName.value=''; stockPrice.value=0; stockQty.value=0\">Save</button>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class='text-success'><b>{{notice}}</b></div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"clearfix\"> </div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>"

/***/ }),

/***/ "../../../../../src/app/stocks/stocks.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StocksComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__stock_service__ = __webpack_require__("../../../../../src/app/stock.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StocksComponent = /** @class */ (function () {
    function StocksComponent(stockService) {
        this.stockService = stockService;
        this.title = "Stock List";
        this.notice = '';
    }
    StocksComponent.prototype.ngOnInit = function () {
        this.getStocks();
    };
    StocksComponent.prototype.getStocks = function () {
        var _this = this;
        this.stockService.getStocks()
            .subscribe(function (stocks) { return _this.stocks = stocks; });
    };
    StocksComponent.prototype.add = function (name, currentPrice, quantity) {
        var _this = this;
        name = name.trim();
        if (!name || !currentPrice || !quantity) {
            return;
        }
        this.stockService.addStock({ name: name, quantity: quantity, currentPrice: currentPrice })
            .subscribe(function (stock) {
            _this.notice = "Stock '" + stock.name + "' added!";
            _this.stocks.push(stock);
        });
    };
    StocksComponent.prototype.delete = function (stock) {
        this.stocks = this.stocks.filter(function (h) { return h !== stock; });
        this.stockService.deleteStock(stock).subscribe();
    };
    StocksComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-stocks',
            template: __webpack_require__("../../../../../src/app/stocks/stocks.component.html"),
            styles: [__webpack_require__("../../../../../src/app/stocks/stocks.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__stock_service__["a" /* StockService */]])
    ], StocksComponent);
    return StocksComponent;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map