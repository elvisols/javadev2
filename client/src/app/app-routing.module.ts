import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StocksComponent }      from './stocks/stocks.component';
import { StockDetailComponent }  from './stock-detail/stock-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/stocks', pathMatch: 'full' },
  { path: 'detail/:id', component: StockDetailComponent },
  { path: 'stocks', component: StocksComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
