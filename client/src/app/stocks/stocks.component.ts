import { Component, OnInit } from '@angular/core';

import { Stock } from '../stock';
import { StockService } from '../stock.service';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {

	title = "Stock List";
	notice = '';
	stocks: Stock[];

	constructor(private stockService: StockService) { }

	ngOnInit() {
		this.getStocks();
	}

	getStocks(): void {
		this.stockService.getStocks()
		.subscribe(stocks => this.stocks = stocks);
	}

	add(name: string, currentPrice: number, quantity: number): void {
		name = name.trim();
		if (!name || !currentPrice || !quantity) { return; }
		this.stockService.addStock({ name, quantity, currentPrice } as Stock)
		.subscribe(stock => {
			this.notice = "Stock '"+ stock.name + "' added!";
			this.stocks.push(stock);
		});
	} 

	delete(stock: Stock): void {
		this.stocks = this.stocks.filter(h => h !== stock);
		this.stockService.deleteStock(stock).subscribe();
	}

}
