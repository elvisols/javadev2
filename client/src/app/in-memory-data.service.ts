import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const stocks = [
      { id: 9, name: 'Tom tom', quantity: 13, currentPrice: 23.4, lastUpdate: '2018-03-08 1:25' },
      { id: 10, name: 'Rice', quantity: 54, currentPrice: 123.4, lastUpdate: '2018-03-08 10:11' },
      { id: 11, name: 'Beans', quantity: 20, currentPrice: 223.4, lastUpdate: '2018-03-08 12:40' },
      { id: 12, name: 'Vegetables', quantity:44, currentPrice: 53.4, lastUpdate: '2018-03-08 6:22' },
      { id: 13, name: 'Cereal', quantity: 23, currentPrice: 103.4, lastUpdate: '2018-03-08 8:35' },
      { id: 14, name: 'Beverages', quantity: 1, currentPrice: 93.4, lastUpdate: '2018-03-08 7:09' },
      { id: 15, name: 'Pepper Soup', quantity: 17, currentPrice: 31.4, lastUpdate: '2018-03-08 11:12' },
      { id: 16, name: 'Corn Beef', quantity: 43, currentPrice: 30.4, lastUpdate: '2018-03-08 9:45' },
      { id: 17, name: 'Semovita', quantity: 2, currentPrice: 10.4, lastUpdate: '2018-03-08 3:45' },
      { id: 18, name: 'Sugar', quantity: 6, currentPrice: 320.4, lastUpdate: '2018-03-08 12:15' },
      { id: 19, name: 'Butter Flour', quantity: 12, currentPrice: 923.4, lastUpdate: '2018-03-08 1:05' },
      { id: 20, name: 'Chinese Rice', quantity: 0, currentPrice: 133.4, lastUpdate: '2018-03-08 12:45' }
    ];
    return {stocks};
  }
  
}
