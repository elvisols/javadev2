import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Stock } from './stock';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Basic ' + btoa("client:secret") })
};

@Injectable()
export class StockService {

	private stocksUrl = 'api/stocks';
  	
  	constructor( private http: HttpClient) { }

  	/** GET stocks from the server */
  getStocks (): Observable<Stock[]> {
    return this.http.get<Stock[]>(this.stocksUrl)
      .pipe(
        tap(stocks => console.log('Stocks fetched: '+JSON.stringify(stocks))),
        catchError(this.handleError('getStockes', []))
      );
  }

  /** POST: add a new stock to the server */
  addStock (stock: Stock): Observable<Stock> {
    return this.http.post<Stock>(this.stocksUrl, stock, httpOptions).pipe(
      tap((stock: Stock) => console.log("id=${stock.id} stock added")),
      catchError(this.handleError<Stock>('addStock'))
    );
  }

  /** DELETE: delete the stock from the server */
  deleteStock (stock: Stock | number): Observable<Stock> {
    const id = typeof stock === 'number' ? stock : stock.id;
    const url = `${this.stocksUrl}/${id}`;

    return this.http.delete<Stock>(url, httpOptions).pipe(
      tap(_ => console.log('deleted stock id=${id}')),
      catchError(this.handleError<Stock>('deleteStock'))
    );
  }

  /** PUT: update the stock on the server */
  updateStock (stock: Stock): Observable<any> {
	const url = `${this.stocksUrl}/${stock.id}`;
    return this.http.put(url, stock, httpOptions).pipe(
      tap(_ => console.log('updated stock id=${stock.id}')),
      catchError(this.handleError<any>('updateStock'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(operation + ' failed: ' + error.message);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

   /** GET stock by id. Will 404 if id not found */
  getStock(id: number): Observable<Stock> {
    const url = `${this.stocksUrl}/${id}`;
    return this.http.get<Stock>(url).pipe(
      tap(_ => console.log(`fetched stock id=${id}`)),
      catchError(this.handleError<Stock>("getStock id=${id}"))
    );
  }

}
