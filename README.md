##	Software engineer application

 Little description

- All endpoints are exposed on (**Port: 8111**).


######Application Requirements:
- Java 8
- Maven 3+

######Deploying and Running application:
> This should be done from the root directory.
```
~$ mvn clean package spring-boot:run
```
Once the service is up, the application can be accessed on port 8111.

				
##   Cheers! 

